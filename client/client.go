package main

import (

	"context"
	email_notify "dauka_notify/protos"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"os"

)


func doLongGreet(c email_notify.NotificationServiceClient) {

	requests := email_notify.NotificationRequest{To: "kanabeesss@gmail.com"}

	ctx := context.Background()
	res, err := c.SingleNotify(ctx, &requests)
	if err != nil {
		log.Fatalf("error while calling LongGreet: %v", err)
	}

	if res.IsDone{
		fmt.Println("works")
	}else{
		fmt.Println("not works")
	}
}







func main() {


	fmt.Println("Hello I'm a client")

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := email_notify.NewNotificationServiceClient(conn)

	doLongGreet(c)
	os.Exit(1)
}
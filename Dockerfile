FROM golang
ADD . /usr/src/service-1
WORKDIR /usr/src/service-1/server
CMD ["go", "run", "."]
package main

import (
	"context"
	email_notify "dauka_notify/protos"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/smtp"
)

type Server struct {
	email_notify.UnimplementedNotificationServiceServer
}



func (s *Server) SingleNotify(ctx context.Context, req *email_notify.NotificationRequest) (*email_notify.NotificationResponse, error) {
	fmt.Printf("Greet function was invoked with %v \n", req)
	email_toSend := req.GetTo()
	fmt.Println(email_toSend)
	summ := req.GetSum()

	if send("you have order some stuff\n" + summ, email_toSend){
		res := email_notify.NotificationResponse{IsDone: true}

		return &res, nil
	}else {
		res := email_notify.NotificationResponse{IsDone: false}
		return &res, nil
	}


}


func send(body string, to string) bool{
	from := "dauka7977@gmail.com"
	pass := "8888dauren"


	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: New Addition!\n\n" +
		body

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return false
	}else{
		log.Print("sent, to " + to + " from " + from )
		return true
	}
}


func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	email_notify.RegisterNotificationServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}

}
